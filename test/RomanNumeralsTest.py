from unittest import TestCase

from romannumerals.RomanNumerals import to_roman_numeral


class RomanNumeralsTest(TestCase):

    def test_shouldTranslate1ToI(self):
        roman_numeral = to_roman_numeral(1)

        self.assertEqual("I", roman_numeral)

    def test_shouldTranslate2ToII(self):
        roman_numeral = to_roman_numeral(2)

        self.assertEqual("II", roman_numeral)

    def test_shouldTranslate5ToV(self):
        roman_numeral = to_roman_numeral(5)

        self.assertEqual("V", roman_numeral)

    def test_shouldTranslate6ToVI(self):
        roman_numeral = to_roman_numeral(6)

        self.assertEqual("VI", roman_numeral)

    def test_shouldTranslate10ToX(self):
        roman_numeral = to_roman_numeral(10)

        self.assertEqual("X", roman_numeral)

    def test_shouldTranslate4ToIV(self):
        roman_numeral = to_roman_numeral(4)

        self.assertEqual("IV", roman_numeral)

    def test_shouldTranslate9ToIX(self):
        roman_numeral = to_roman_numeral(9)

        self.assertEqual("IX", roman_numeral)

    def test_shouldTranslate19ToXIX(self):
        roman_numeral = to_roman_numeral(19)

        self.assertEqual("XIX", roman_numeral)

    def test_shouldTranslate50ToL(self):
        roman_numeral = to_roman_numeral(50)

        self.assertEqual("L", roman_numeral)

    def test_shouldTranslate100ToC(self):
        roman_numeral = to_roman_numeral(100)

        self.assertEqual("C", roman_numeral)

    def test_shouldTranslate500ToD(self):
        roman_numeral = to_roman_numeral(500)

        self.assertEqual("D", roman_numeral)

    def test_shouldTranslate1000ToM(self):
        roman_numeral = to_roman_numeral(1000)

        self.assertEqual("M", roman_numeral)

    def test_shouldTranslate41ToXLI(self):
        roman_numeral = to_roman_numeral(41)

        self.assertEqual("XLI", roman_numeral)

    def test_shouldTranslate44ToXLIV(self):
        roman_numeral = to_roman_numeral(44)

        self.assertEqual("XLIV", roman_numeral)

    def test_shouldTranslate99ToXCIX(self):
        roman_numeral = to_roman_numeral(99)

        self.assertEqual("XCIX", roman_numeral)

    def test_shouldTranslate999ToCMXCIX(self):
        roman_numeral = to_roman_numeral(999)

        self.assertEqual("CMXCIX", roman_numeral)

    def test_shouldTranslate3497ToMMMCDXCVII(self):
        roman_numeral = to_roman_numeral(3497)

        self.assertEqual("MMMCDXCVII", roman_numeral)
