"""
' Function for converting integers to roman numerals
'
"""


def to_roman_numeral(num):
    symbols = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1]
    numerals = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I']
    roman_numerals = ''

    i = 0

    while num > 0:
        for _ in range(num // symbols[i]): # divides the number with the symbol on index[i]
            roman_numerals += numerals[i] #adds the numeral value to function roman
            num -= symbols[i] #subtracts the symbols to the number until we cannot divide anymore
        i += 1

    return roman_numerals


