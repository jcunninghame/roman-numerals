"""
' Roman Numerals
'
"""
import sys

from romannumerals.RomanNumerals import to_roman_numeral


def main(argv):
    to_roman_numeral(argv[1])


''' Main, takes argument of data file '''
if __name__ == "__main__":
    main(sys.argv)
